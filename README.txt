
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installing


INTRODUCTION
------------

Kalatheme Everywhere combines the Kalatheme base theme with the layout
capabilities of Panels Everywhere. It provides a simple site template to
kickstart the use of those two components together.


INSTALLING
----------

The following dependencies exist:

 * Kalatheme
 * Panels Everywhere

After creating a sub-theme and setting up Panels Everywhere the supplied
"Straight Up" site template variant will be enabled by default.
