<?php
/**
 * @file
 * Primary pre/preprocess functions and alterations.
 */

/**
 * Implements hook_preprocess_HOOK() for pane_header.tpl.php.
 */
function kalatheme_everywhere_preprocess_pane_header(&$variables) {
  // Add menu variables.
  $variables['main_menu'] = theme_get_setting('toggle_main_menu') ? menu_main_menu() : array();
  $variables['secondary_menu'] = theme_get_setting('toggle_secondary_menu') ? menu_secondary_menu() : array();

  // Add Kalatheme page variables.
  kalatheme_process_page($variables);
}

/**
 * Implements hook_preprocess_HOOK() for straight_up.tpl.php.
 */
function kalatheme_everywhere_preprocess_straight_up(&$variables) {
  // If panels aren't being used at all.
  $variables['no_panels'] = !(module_exists('page_manager') && page_manager_get_current_page());
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function kalatheme_everywhere_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  $dropdown_attributes = array(
    'container' => array(
      'class' => array('dropdown', 'actions', 'pull-right'),
    ),
    'toggle' => array(
      'class' => array('dropdown-toggle', 'enabled'),
      'data-toggle' => array('dropdown'),
      'href' => array('#'),
    ),
    'content' => array(
      'class' => array('dropdown-menu'),
    ),
  );

  // Copy the action links into the tabs section.
  if ($data['actions']['count'] > 0) {
    $data['tabs'][0]['count'] += 1;
    $data['tabs'][0]['output'][] = array(
      '#theme' => 'menu_local_actions',
      '#menu_actions' => $data['actions']['output'],
      '#attributes' => $dropdown_attributes,
    );
  }

  // Remove the action links.
  $data['actions']['count'] = 0;
  $data['actions']['output'] = array();
}
